import 'package:flutter/material.dart';

class SettingsView extends StatelessWidget {
  const SettingsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width > 500? MediaQuery.of(context).size.width * 0.25 : MediaQuery.of(context).size.width * 0.1),
        width: MediaQuery.of(context).size.width > 500? MediaQuery.of(context).size.width * 0.5 : MediaQuery.of(context).size.width * 0.8,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            ListTile(
              leading: const Icon(Icons.notifications),
              title: const Text('Push notifications'),
              trailing: Switch(value: true, onChanged: (_) {}),
            ),
          ],
        ),
      ),
    );
  }
}