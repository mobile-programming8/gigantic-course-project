import 'package:flutter/material.dart';

import 'app_menu.dart';
import 'drawer.dart';
import 'course_view.dart';
import 'data/course.dart';
import 'app_layout.dart';
import 'courses.dart';

class CourseList extends StatelessWidget {
  const CourseList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final smallerDevice = width < AppLayout.widthFirstBreakpoint;

    // final courseWrap = SingleChildScrollView(
    //   child: Wrap(
    //     children: Course.shortList
    //         .map(
    //             (course) => Container(
    //       constraints: const BoxConstraints(
    //         maxWidth: AppLayout.widthFirstBreakpoint),
    //       child: CourseView(course: course),
    //     ),
    //     ).toList(),
    //   ),
    // );

    // final courseGrid = GridView.extent(maxCrossAxisExtent: 450,
    //   childAspectRatio: 0.8,
    //   children: Course.shortList
    //     .map(
    //           (course) => CourseView(course: course),
    //   ).toList(),
    // );
    //

    const courseGrid = Courses();

    final courseList = Padding(
      padding: const EdgeInsets.all(8.0),
      child: courseGrid,
    );

    return Scaffold(
      backgroundColor: Colors.green[900],
      appBar: AppBar(
        title: const Text('Not a fully responsive app'),
      ),
      drawer: smallerDevice ? const AppDrawer() : null,
      body: smallerDevice
          ? courseList
          : Row(
              children: [
                const Expanded(
                  flex: 1,
                  child: AppMenu(),
                ),
                Expanded(
                  flex: 6,
                  child: courseList,
                ),
              ],
            ),
    );
  }
}
